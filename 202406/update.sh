#!/bin/bash

DOWNLOAD_SCRIPT_URL="https://gitlab.com/wingbits/config/-/raw/master/download.sh"
UPDATE_SCRIPT_URL="https://gitlab.com/wingbits/config/-/raw/master/update.sh"
CRON_IDENTIFIER="wingbits_config_update"
LOG_FILE="/var/log/wb_update.log"


# Throw warning if script is not executed as root
if [[ $EUID -ne 0 ]]; then
    echo "ERROR: This script must be run as root"
    echo "Run it like this:"
    echo "sudo ./update.sh"
    exit 1
fi

local_version=$(cat /etc/wingbits/version)
script=$(curl -s $DOWNLOAD_SCRIPT_URL)
version=$(echo "$script" | grep -oP '(?<=WINGBITS_CONFIG_VERSION=")[^"]*')

version_major_minor=$(echo "$version" | cut -d'.' -f1-2)
local_version_major_minor=$(echo "$local_version" | cut -d'.' -f1-2)

echo "--------------------------"
echo "$(date)"
echo "Current local version: $local_version"
echo "Latest available Wingbits version: $version"

### handle cron frequency updates ###
# Remove the old cron job with the unique identifier, if it exists
if crontab -l | grep -q "$CRON_IDENTIFIER"; then
    (crontab -l | grep -v "$CRON_IDENTIFIER") | crontab -
fi

# Add the new cron job  
UPDATE_JOB="0 18 * * * /usr/bin/curl -s $UPDATE_SCRIPT_URL | /bin/bash >> $LOG_FILE 2>&1 # $CRON_IDENTIFIER"
(crontab -l ; echo "$UPDATE_JOB") | crontab -

if [[ ! -e /etc/vector/vector.yaml ]] || [[ ! -e /etc/default/vector ]]; then
	
	/usr/bin/curl -sL https://gitlab.com/accigue/config/-/raw/master/fix_yaml.sh | bash
	
fi