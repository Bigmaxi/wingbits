#!/bin/bash

# SCRIPT_URL="https://gitlab.com/wingbits/config/-/raw/master/wb_install.sh" ################# Change back after testing
SCRIPT_URL="https://gitlab.com/Bigmaxi/wingbits/-/raw/main/wb_install.sh"


# Throw warning if script is not executed as root
if [[ $EUID -ne 0 ]]; then
    echo "ERROR: This script must be run as root"
    echo "Run it like this:"
    echo "sudo ./wb_update.sh"
    exit 1
fi

local_version=$(cat /etc/wingbits/version)
script=$(curl -s $SCRIPT_URL)
version=$(echo "$script" | grep -oP '(?<=WINGBITS_CONFIG_VERSION=")[^"]*')

version_major_minor=$(echo "$version" | cut -d'.' -f1-2)
local_version_major_minor=$(echo "$local_version" | cut -d'.' -f1-2)

echo "--------------------------"
echo "$(date)"
echo "Current local version: $local_version"
echo "Latest available Wingbits version: $version"

if [ "$version_major_minor" != "$local_version_major_minor" ]; then
    echo "Updating wingbits..."
    curl -sL $SCRIPT_URL | sudo bash -c 'bash /dev/stdin --read-from-file'
else
    echo "No update required"
    exit 0
fi

