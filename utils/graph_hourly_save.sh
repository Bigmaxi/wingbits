#!/bin/bash

# Excerpt of install script to set graph save to hourly from once a day
# 2024-11-14

# Ensure the script is run as root
if [[ $EUID -ne 0 ]]; then
    echo "ERROR: This script must be run as root to set up the service correctly"
    echo "Run it like this:"
    echo "sudo ./download.sh"
    exit 1
fi


# Function to change the collectd restart time to every hour from once per day so graphs are written to disk hourly
function collectd_restart() {

	# Define cron file path
	FILE="/etc/cron.d/collectd_to_disk"

	# Define the new cron job line
	NEW_CRON_JOB="42 */1 * * * root /bin/systemctl restart collectd"

	if grep -Fxq "$NEW_CRON_JOB" "$FILE"; then
		echo "collectd restart already at 1hr"
	else
		# Comment out the old cron job and add the new one
		sed -i 's/^42 23 \* \* \* root \/bin\/systemctl restart collectd/# 42 23 * * * root \/bin\/systemctl restart collectd/' "$FILE"
		echo "" >> "$FILE"
		echo "# every 1 hour" >> "$FILE"
		echo "$NEW_CRON_JOB" >> "$FILE"

		# Verify the changes
		echo "Modified collectd restart cron file:"
		cat "$FILE"
	fi
}


collectd_restart