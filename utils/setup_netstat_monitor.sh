#!/bin/bash

# Define the path to the netstat_monitor.sh file
SCRIPT_NAME="netstat_monitor.sh"
SCRIPT_PATH="$HOME/$SCRIPT_NAME"

# Create the netstat_monitor.sh file with the provided content
cat << 'EOF' > "$SCRIPT_PATH"
#!/bin/bash

#Quick script to monitor the vector network sockets open and restart vector if it is not cleaning old ones up.
#Created by BM. Last Mod: 2024-07-02

# Configurable threshold
threshold=6

# Run the netstat command and count occurrences of 'vector' or 'readsb'
count=$(sudo netstat -tpn | grep -Ec 'vector|readsb')

# Write current date/time
echo "$(date '+%Y-%m-%d %H:%M:%S'): Script ran - Count $count" | sudo tee -a /var/log/netstat_monitor.log

# Check if count is greater than the threshold
if [ $count -gt $threshold ]; then
    # Write netstat output to log file
    sudo netstat -tpn | grep -E 'vector|readsb' | sudo tee -a /var/log/netstat_monitor.log

    # Stop vector service
    sudo systemctl stop vector

    # Wait for 1 minute
    sleep 60

    # Start vector service
    sudo systemctl start vector
fi
EOF

# Set the script to be executable
chmod +x "$SCRIPT_PATH"

# Remove the old cron job with the unique identifier, if it exists
if crontab -l | grep -q "netstat_monitor"; then
    (crontab -l | grep -v "netstat_monitor") | crontab -
fi

# Create a crontab entry to execute the script every 30 minutes
(crontab -l ; echo "*/30 * * * * $SCRIPT_PATH # netstat_monitor") | crontab -
