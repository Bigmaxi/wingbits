#!/bin/bash

# Define the file path
FILE_PATH="/lib/systemd/system/vector.service"


# Throw warning if script is not executed as root
if [[ $EUID -ne 0 ]]; then
    echo "ERROR: This script must be run as root via sudo"
    exit 1
fi

# Check if the file exists
if [ ! -f "$FILE_PATH" ]; then
    echo "Service file not found! Vector must not be installed?"
    exit 1
fi

# Check if 'RestartSec=' is already present in the file
if grep -q "^RestartSec=" "$FILE_PATH"; then

    echo "'RestartSec=' is already present in the file."
	
else
	# Insert the line above the line containing 'StartLimitInterval=10'
	sed -i '/StartLimitInterval/i RestartSec=900' "$FILE_PATH"

	# Reload the systemd manager configuration
	systemctl daemon-reload

	# Restart the vector service
	echo ""
	echo "Starting/restarting vector..."
	systemctl restart vector.service

	echo ""
	echo "RestartSec added and vector service restarted."
	echo ""
	echo "If errors are shown from restarting vector, please add the output of this script in your ticket"

fi
