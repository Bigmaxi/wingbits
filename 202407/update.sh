#!/bin/bash

DOWNLOAD_SCRIPT_URL="https://gitlab.com/wingbits/config/-/raw/master/download.sh"
UPDATE_SCRIPT_URL="https://gitlab.com/wingbits/config/-/raw/master/update.sh"
VECTOR_SERVICE_PATH="/lib/systemd/system/vector.service"
CRON_IDENTIFIER="wingbits_config_update"
LOG_FILE="/var/log/wingbits/wb_update.log"

# Throw warning if script is not executed as root
if [[ $EUID -ne 0 ]]; then
    echo "ERROR: This script must be run as root"
    echo "Run it like this:"
    echo "sudo ./update.sh"
    exit 1
fi

local_version=$(cat /etc/wingbits/version)
script=$(curl -s $DOWNLOAD_SCRIPT_URL)
version=$(echo "$script" | grep -oP '(?<=WINGBITS_CONFIG_VERSION=")[^"]*')

version_major_minor=$(echo "$version" | cut -d'.' -f1-2)
local_version_major_minor=$(echo "$local_version" | cut -d'.' -f1-2)

echo "--------------------------"
echo "$(date)"
echo "Current local version: $local_version"
echo "Latest available Wingbits version: $version"

### handle cron frequency updates ###
# Remove the old cron job with the unique identifier, if it exists
if crontab -l | grep -q "$CRON_IDENTIFIER"; then
    (crontab -l | grep -v "$CRON_IDENTIFIER") | crontab -
fi

# Add the new cron job
mkdir -p /var/log/wingbits
UPDATE_JOB="0 18 * * * /usr/bin/curl -s $UPDATE_SCRIPT_URL | /bin/bash >> $LOG_FILE 2>&1 # $CRON_IDENTIFIER"
(crontab -l ; echo "$UPDATE_JOB") | crontab -

# If the vector config file is missing or 0 bytes run the script to correct
if [[ ! -e /etc/vector/vector.yaml || ! -s /etc/vector/vector.yaml ]]; then
	
    curl -sL https://gitlab.com/wingbits/config/-/raw/master/fix_yaml.sh | bash
	
fi

# Add in the restartsec option to the Vector service if not present
if [[ -e /lib/systemd/system/vector.service ]]; then
	if ! grep -q "^RestartSec=" "$VECTOR_SERVICE_PATH"; then

		# Insert above the line containing 'StartLimitInterval='
		sed -i '/StartLimitInterval/i RestartSec=900' "$VECTOR_SERVICE_PATH"

		# Reload the systemd manager configuration and restart vector
		systemctl daemon-reload
		systemctl restart vector.service

		echo ""
		echo "RestartSec added and Vector service restarted."
	fi	
fi
