#!/bin/bash

#20240429 BM - Below exerpt checks for existing Lat/Long in readsb config and allows new on to be entered.
# Added in test for passing in Lat/Long and ID when script is run


# Function to validate latitude
function validate_latitude() {
#    local lat_regex='^[-+]?([0-8]?[0-9]|90)(\.[0-9]{3,})?$'
	local lat_regex='^[-+]?([0-9]|[1-8][0-9])(\.[0-9]+)?$'
    if [[ $1 =~ $lat_regex ]]; then
        echo "Latitude is valid ($1)"
		return 0
    else
        echo "Latitude is not valid ($1)"
		return 1
    fi
}

# Function to validate longitude
function validate_longitude() {
    local long_regex='^[-+]?((1?[0-7]?[0-9])|180)(\.[0-9]{3,})?$' #fails with +-95
#	local lon_regex='^[-+]?([0-9]|[1-9][0-9]|1[0-7][0-9]|180)(\.[0-9]+)?$' # allows -+190
    if [[ $1 =~ $long_regex ]]; then
        echo "Longitude is valid ($1)"
		return 0
    else
        echo "Longitude is not valid ($1)"
		return 1
    fi
}

# Function to location
function validate_location() {
    local loc_regex='^((\-?|\+?)?\d+(\.\d+)?),\s*((\-?|\+?)?\d+(\.\d+)?)$'
    if [[ $1 =~ $loc_regex ]]; then
        echo "Location is valid ($1)"
		return 0
    else
        echo "Location is not valid ($1)"
		return 1
    fi
}

# Function to validate coordinates
check_coordinates() {   
    readsb_file="/etc/default/readsb"
    latitude="none"
    longitude="none"
    local lat_regex='^[-+]?([0-9]|[1-8][0-9])(\.[0-9]+)?$'
    local lon_regex='^[-+]?([0-9]|[1-9][0-9]|1[0-7][0-9]|180)(\.[0-9]+)?$'
    
   
#	latitude=$(echo "$1" | cut -d ',' -f1)
#	longitude=$(echo "$1" | cut -d ',' -f2 | tr -d ' ')
	
	latitude=$(echo $1 | tr -cd '[:digit:].-')
	longitude=$(echo $2 | tr -cd '[:digit:].-')


	# Validate latitude from -90 to +90
	if [[ $latitude =~ $lat_regex && $(awk -v lat="$latitude" 'BEGIN {if (lat >= -90 && lat <= 90) print 1; else print 0}') -eq 1 ]]; then
		echo -e "\033[0;32m✓\033[0m Valid coordinate for latitude: $latitude"
		
	else
		echo -e "\033[0;31m✗\033[0m Invalid coordinate for latitude: $latitude"
		return 1
	fi

	# Validate longitude from -180 to +180
	if [[ $longitude =~ $lon_regex && $(awk -v lon="$longitude" 'BEGIN {if (lon >= -180 && lon <= 180) print 1; else print 0}') -eq 1 ]]; then
		echo -e "\033[0;32m✓\033[0m Valid coordinate for longitude: $longitude"
		
	else
		echo -e "\033[0;31m✗\033[0m Invalid coordinate for longitude: $longitude"
		return 1
	fi
}



# Function to enter location
function set_location() {
	readsb_file="/etc/default/readsb"
	readsb_antenna_location="none"
	# Get current lat long from readsb config
	if [[ -e $readsb_file ]]; then
			readsb_lat=$(grep -Po -- '--lat \K-?\d+\.\d+' "$readsb_file")
			readsb_lon=$(grep -Po -- '--lon \K-?\d+\.\d+' "$readsb_file")
			readsb_antenna_location="$readsb_lat, $readsb_lon"
			echo "$readsb_lat, $readsb_lon"
			echo $readsb_antenna_location
			if [ "$readsb_antenna_location" = "none" ]; then
				echo "Lat/Long not found in readsb config file"
			fi
	else
        echo -e "No readsb file found."
	fi		
	
	while true; do
		read -p "Enter the location from antennas page eg. -31.961672, 115.831493 (current: $readsb_antenna_location): " antenna_location </dev/tty

		# Use the current value if the user presses enter without typing anything
		antenna_location=${antenna_location:-$readsb_antenna_location}
		
		# Split the string into two variables
		latitude=$(echo "$antenna_location" | cut -d ',' -f1)
		longitude=$(echo "$antenna_location" | cut -d ',' -f2 | tr -d ' ')
		
		# Validate the location
#		if validate_latitude "$latitude" && validate_longitude "$longitude"; then
#			echo "$antenna_location is valid"
#			break
#		else	
#			echo "Invalid format ($antenna_location)"
#		fi

# Validate the location
		if check_coordinates "$antenna_location"; then
			echo "$antenna_location is valid"
			break
		else	
			echo "Invalid format ($antenna_location)"
		fi

	done
			
	
	
}

echo $wb_ant_loc
echo $wb_ant_id

echo "wb_ant_loc: $wb_ant_loc"
echo "wb_ant_id: $wb_ant_id"
echo "wb_ant_test1: $wb_ant_test1"
echo "wb_ant_test2: $wb_ant_test2"

# Access the first variable
first_var="$1"
echo "First variable: $first_var"

# Access the second variable
second_var="$2"
echo "Second variable: $second_var"

# set_location



