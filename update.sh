#!/bin/bash


INSTALL_CURRENT_VERSION="0.1.0"
INSTALL_CLIENT_SCRIPT_URL="https://gitlab.com/Bigmaxi/wingbits/-/raw/main/install-client.sh"         #************ Change to Prod *********  https://gitlab.com/wingbits/config/-/raw/master/install-client.sh
UPDATE_SCRIPT_URL="https://gitlab.com/wingbits/config/-/raw/master/update.sh"
CRON_IDENTIFIER="wingbits_config_update"
LOG_FILE="/var/log/wingbits/wb_update.log"

# Throw warning if script is not executed as root
if [[ $EUID -ne 0 ]]; then
    echo "ERROR: This script must be run as root"
    echo "Run it like this:"
    echo "sudo ./update.sh"
    exit 1
fi

echo "--------------------------"
echo "$(date)"


### handle cron frequency updates ###

# Remove the old cron job with the unique identifier, if it exists
if crontab -l | grep -q "$CRON_IDENTIFIER"; then
    (crontab -l | grep -v "$CRON_IDENTIFIER") | crontab -
fi

# Add the new cron job
mkdir -p /var/log/wingbits
UPDATE_JOB="0 18 * * * /usr/bin/curl -s $UPDATE_SCRIPT_URL | /bin/bash >> $LOG_FILE 2>&1 # $CRON_IDENTIFIER"
(crontab -l ; echo "$UPDATE_JOB") | crontab -

### update wb-config

# extract the version from a given file
extract_wbconfig_version() {
    grep -oP 'VERSION=\K[\d.]+' "$1"
}

update_wbconfig() {
    local tempfile=$(mktemp)
    local remote_url="https://gitlab.com/wingbits/config/-/raw/master/wb-config/wb-config"
    local local_file="/usr/local/bin/wb-config"

    # If the wb-config config file is missing, install it
    if [[ ! -e "$local_file" ]]; then
	echo "Local wb-config not installed.  Installing..."
        curl -sL https://gitlab.com/wingbits/config/-/raw/master/wb-config/install.sh | bash

        # no need to update as the above would install the latest version, so exit function
        return 0
    fi
    
    # Download the file to a temporary location
    curl --fail -s -o "$tempfile" "$remote_url"

    # Check if the download was successful
    if [[ $? -ne 0 ]]; then
        echo "Failed to download the latest wb-config for version comparison."
        rm "$tempfile"
    else
        # extract versions from the downloaded and local files
        downloaded_version=$(extract_wbconfig_version "$tempfile")
        local_version=$(extract_wbconfig_version "$local_file")

	# set default value for local_version if value missing in file
        if [ -z "$local_version" ]; then
	    echo "wb-config missing version, must be old format, defaulting to v0.0.0"
            local_version="0.0.0"
        fi

        # compare versions and replace file if they don't match
        if [ "$downloaded_version" != "$local_version" ]; then
            echo "Replacing wb-config (v${local_version}) with the latest available (v${downloaded_version})."
            curl -sL https://gitlab.com/wingbits/config/-/raw/master/wb-config/install.sh | bash
        else
            echo "Local wb-config is up-to-date. No action taken."
            rm "$tempfile"
        fi
    fi
}

# Install the Wingbits Client if it is missing or not at lastest version
install_wbclient () {

	local INSTALL_LOCAL_VERSION=$(cat /etc/wingbits/version)

	#Check if local version is current or binary is missing 
	if [ "$INSTALL_CURRENT_VERSION" != "$INSTALL_LOCAL_VERSION" ] || [ ! -x "/usr/local/bin/wingbits" ]; then               #***** Could not do greater than as they are strings. Do we need to convert first?
		echo																												#******Hardcoded path as command does not want to run when launched from crontab...
		echo "Current install version: $INSTALL_LOCAL_VERSION"
		echo "Latest install version: $INSTALL_CURRENT_VERSION"
		echo
		curl -sL $INSTALL_CLIENT_SCRIPT_URL | bash
		echo
	else
		echo "Local Wingbits client install is up-to-date. No action taken."
	fi

	
}

# check for updated wb-config
update_wbconfig

# check Wingbits Client installed
install_wbclient

### Bug related updates/fixes

# none at this time
