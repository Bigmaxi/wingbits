#!/bin/bash

# Official install script for wingbits - created/managed by wingbits team
WINGBITS_CONFIG_VERSION="0.0.03"
# 2024-05

################## Update below (gitlab) to final location and the location for cron job and vector config file...##################################

# Example if pulling install down from Gitlab: curl -sL https://gitlab.com/Bigmaxi/wingbits/-/raw/main/wb_install.sh | sudo heatmap=true initial=true loc="-31.966645, 115.862013" id="test-test-test" bash
# Example running locally: sudo heatmap=true initial=true loc="-31.966645, 115.862013" id="test-test-test" ./wb_install.sh
# Where: heatmap=true is used to signal that you want to enable the heatmap config
#		initial=true can be set to perform the same install as if there was not already an existing Wingbits install present (ie. /etc/wingbits directory does not exist)
#		loc="-31.966645, 115.862013" is the location lat/long to be used
#		id="test-test-test" is the antenna ID to be used
# Can still be run with no values set in the cmd line and will be prompted. Eg. curl -sL https://gitlab.com/Bigmaxi/wingbits/-/raw/main/wb_install.sh | sudo bash


# Throw warning if script is not executed as root
if [[ $EUID -ne 0 ]]; then
    echo "ERROR: This script must be run as root to set up the service correctly"
    echo "Run it like this:"
    echo "sudo ./wb_install.sh"
    exit 1
fi

# Function to get input for install actions and other prep
function install_prep() {

	# Read in values passed in from cmd line
	ant_loc=$loc
	ant_id=$id
	
	# Check if this is an initial WB install by checking if the /etc/wingbits dir exists or $initial value set to true from cmd line
	if [ ! -d "/etc/wingbits" ] || [ "$initial" = true ]; then
		initial_install=true
	else
		initial_install=false
	fi
	
	current_datetime=$(date +'%Y-%m-%d_%H-%M')
	# Change to user's home dir
	user_dir=$(getent passwd ${SUDO_USER:-$USER} | cut -d: -f6)
	cd $user_dir
	# Write start date/time to log					 	
	logfile="${user_dir}/wingbits_${current_datetime}.log"
	echo "$(date): Wingbits install start time" >> $logfile
	
	# Write the OS details to log file
	cat /etc/os-release >> $logfile

	# Create config directory doesn't exist
	mkdir -p /etc/wingbits


	# Prompt to ugrade packages
		while true; do
		# Prompt user for input
		read -rp "Would you like to upgrade installed packages prior to install? (recommended) -  [Y/n] " input </dev/tty

		# Default to "Y" if user presses Enter without typing anything
		input=${input:-Y}

		# Convert input to uppercase
		input=$(echo "$input" | tr '[:lower:]' '[:upper:]')

		# Check if input is "Y" or "N"
		if [ "$input" = "Y" ]; then
			package_update=true
			break
		elif [ "$input" = "N" ]; then
			package_update=false
			break
		else
			echo "Invalid input. Please enter 'Y' or 'N'."
		fi
	done
			
	# Prompt to setup auto-update cron job
	while true; do
		# Prompt user for input
		read -rp "Would you like to have the Wingbits install auto-update when required? (recommended) -  [Y/n] " input </dev/tty

		# Default to "Y" if user presses Enter without typing anything
		input=${input:-Y}

		# Convert input to uppercase
		input=$(echo "$input" | tr '[:lower:]' '[:upper:]')

		# Check if input is "Y" or "N"
		if [ "$input" = "Y" ]; then
			auto_update=true
			break
		elif [ "$input" = "N" ]; then
			auto_update=false
			break
		else
			echo "Invalid input. Please enter 'Y' or 'N'."
		fi
	done
		
	# Rename existing Vector yaml and toml files to avoid issue with Vector UG
	if [[ -e /etc/vector/vector.yaml ]]; then
		mv /etc/vector/vector.yaml /etc/vector/vector.yaml${current_datetime}
	fi
	if [[ -e /etc/vector/vector.toml ]]; then
		mv /etc/vector/vector.toml /etc/vector/vector.toml${current_datetime}
	fi

	# Remove Vector Device ID file to prevent issue with vector upgrade or install
	rm -f /etc/default/vector
	
}


# Function to validate lat/long coordinates
function check_coordinates() {   
    latitude="none"
    longitude="none"
    local lat_regex='^[-+]?([0-9]|[1-8][0-9])(\.[0-9]+)?$'
    local lon_regex='^[-+]?([0-9]|[1-9][0-9]|1[0-7][0-9]|180)(\.[0-9]+)?$'
    
   	latitude=$(echo "$1" | cut -d ',' -f1)
	longitude=$(echo "$1" | cut -d ',' -f2 | tr -d ' ')

	# Validate latitude from -90 to +90
	if [[ $latitude =~ $lat_regex && $(awk -v lat="$latitude" 'BEGIN {if (lat >= -90 && lat <= 90) print 1; else print 0}') -eq 1 ]]; then
		echo -e "\033[0;32m✓\033[0m Valid coordinate for latitude: $latitude"		
	else
		echo -e "\033[0;31m✗\033[0m Invalid coordinate for latitude: $latitude"
		return 1
	fi

	# Validate longitude from -180 to +180
	if [[ $longitude =~ $lon_regex && $(awk -v lon="$longitude" 'BEGIN {if (lon >= -180 && lon <= 180) print 1; else print 0}') -eq 1 ]]; then
		echo -e "\033[0;32m✓\033[0m Valid coordinate for longitude: $longitude"
	else
		echo -e "\033[0;31m✗\033[0m Invalid coordinate for longitude: $longitude"
		return 1
	fi
}


# Function to get location for readsb config
function read_location() {
	
	if [[ -n $ant_loc ]]; then
		if check_coordinates "$ant_loc"; then
			echo "$ant_loc is valid" | tee -a $logfile
			antenna_location=$ant_loc
		else	
			echo "Invalid format ($antenna_location)"
		fi
	
	else
		# Get current lat/long from readsb config
		readsb_file="/etc/default/readsb"
		readsb_antenna_location="none"
		
		if [[ -e $readsb_file ]]; then
			readsb_lat=$(grep -Po -- '--lat \K-?\d+\.\d+' "$readsb_file")
			readsb_lon=$(grep -Po -- '--lon \K-?\d+\.\d+' "$readsb_file")
			readsb_antenna_location="$readsb_lat, $readsb_lon"
			# echo "readsb_lat, readsb_lon: $readsb_lat, $readsb_lon"
			echo "readsb_antenna_location: $readsb_antenna_location" | tee -a $logfile
			
			if [ "$readsb_antenna_location" = "none" ]; then
				echo "Lat/Long not found in readsb config file" >> $logfile 2>&1
			fi
		else
			echo -e "No readsb file found." >> $logfile 2>&1
		fi

		while true; do
			read -p "Enter the location from antennas page eg. -31.961672, 115.831493 (current: $readsb_antenna_location): " antenna_location </dev/tty
		
			# Use the current value if the user presses enter without typing anything
			antenna_location=${antenna_location:-$readsb_antenna_location}
		
			# Validate the location
			if check_coordinates "$antenna_location"; then
				echo "$antenna_location is valid"
				break
			else
				echo "Invalid format ($antenna_location)"
			fi
		done
	fi
}

# Function to validate the Device ID input format
function validate_deviceid() {
    if [[ "$1" =~ ^[a-z]+-[a-z]+-[a-z]+$ ]]; then
        return 0
    else
        echo "Invalid format. Please copy the ID from the antennas dashboard page (three lowercase words separated by hyphens with no spaces)."
        return 1
    fi
}


# Function to read in the Device_ID
function set_device_id() {
	
	if [[ -n $ant_id ]]; then
		# check supplied device id from cmd line is valid
		if validate_deviceid "$ant_id"; then
			echo "$ant_id is valid" | tee -a $logfile
			device_id=$ant_id
		else	
			echo "Invalid format ($ant_id)"
		fi
	else
	
		file_device_id="none"
		# Read in Device ID if already exists
		if [[ -e /etc/wingbits/device ]]; then
			read -r file_device_id < /etc/wingbits/device
		fi
		while true; do
			read -p "Enter the device ID from antennas page (default: $file_device_id): " device_id </dev/tty

			# Use the default value if the user presses enter without typing anything
			device_id=${device_id:-$file_device_id}

			# Validate the device id
			if validate_deviceid "$device_id"; then
				break
			else
				echo "Invalid format ($device_id)"
			fi
		done
	fi
	echo "$device_id" > /etc/wingbits/device
	echo "Using device ID: $device_id" | tee -a $logfile
}


# Function to delete old files that are no longer required
function del_old_files() {

	# Files to delete if present
	files=(
	  "/etc/wingbits/check_status.sh"
	  "/etc/cron.d/wingbits"
	)

	# Loop through each file and if exists, delete it
	for file in "${files[@]}"; do
	  if [ -e "$file" ]; then
		sudo rm "$file"
		if [ $? -eq 0 ]; then
		  echo "$file deleted successfully." >> $logfile 2>&1
		else
		  echo "Failed to delete $file." >> $logfile 2>&1
		fi
	  else
		echo "$file does not exist. No need to delete." >> $logfile 2>&1
	  fi
	done
}


# Function to display loading animation with an airplane icon
function show_loading() {
  local text=$1
  local delay=0.2
  local frames=("⣾" "⣽" "⣻" "⢿" "⡿" "⣟" "⣯" "⣷")
  local frame_count=${#frames[@]}
  local i=0

  while true; do
    local frame_index=$((i % frame_count))
    printf "\r%s  %s" "${frames[frame_index]}" "${text}"
    sleep $delay
    i=$((i + 1))
  done
}


# Function to run multiple commands and log the output
function run_command() {
  local commands=("$@")
  local text=${commands[0]}
  local command
  echo "===================${text}====================" >> $logfile

  for command in "${commands[@]:1}"; do
    (
      eval "${command}" >> $logfile 2>&1
      printf "done" > /tmp/wingbits.done
    ) &
    local pid=$!

    show_loading "${text}" &
    local spinner_pid=$!

    # Wait for the command to finish
    wait "${pid}"

    # Kill the spinner
    kill "${spinner_pid}"
    wait "${spinner_pid}" 2>/dev/null

    # Check if the command completed successfully
    if [[ -f /tmp/wingbits.done ]]; then
      rm /tmp/wingbits.done
      printf "\r\033[0;32m✓\033[0m   %s\n" "${text}"
    else
      printf "\r\033[0;31m✗\033[0m   %s\n" "${text}"
    fi
  done
}


# Function to check if ADSB adapter is plugged in
check_adsb_adapter() {
    local rtl_device=$(lsusb | grep -i "RTL28")
    if [[ -n "$rtl_device" ]]; then
        echo -e "\033[0;32m✓\033[0m ADSB adapter found: $rtl_device" | tee -a $logfile
		return 0
    else
        echo -e "\033[0;31m✗\033[0m No (RTL28xx) ADSB adapter found." | tee -a $logfile
		return 1
    fi
}


function check_service_status(){
	local services=("$@")
	# Only delay and try again if ot initial install and ADSB SDR found
	if [[ $initial_install != true ]] && [[ check_adsb_adapter ]] ; then
	  for service in "${services[@]}"; do
		status="$(systemctl is-active "$service".service)"
		if [ "$status" != "active" ]; then
			echo "$service is inactive. Waiting 30 seconds..."
			sleep 30 # on initial decoder install (readsb) a reboot is required, but should start fine on updates/reinstalls
			status="$(systemctl is-active "$service".service)"
			if [ "$status" != "active" ]; then
				echo "$service is still inactive."
			else
				echo "$service is now active. ✈"
			fi
		else
			echo "$service is active. ✈"
		fi
	  done
	fi
}


# Function to add/update cron to run check for new install script every 5 days
function update_crontab() {

	# Variables
	# local SCRIPT_URL="https://gitlab.com/wingbits/config/-/raw/wb_update.sh"  ################# Change back after testing
	local SCRIPT_URL="https://gitlab.com/Bigmaxi/wingbits/-/raw/main/wb_update.sh"
	local CRON_IDENTIFIER="wingbits_config_update"  # Unique identifier for the cron job
	local LOG_FILE="/var/log/wb_update.log"

	# Remove the old cron job with the unique identifier, if it exists
	if crontab -l | grep -q "$CRON_IDENTIFIER"; then
		(crontab -l | grep -v "$CRON_IDENTIFIER") | crontab -
	fi
	
	# Add the new cron job  							
	if [[ $auto_update = true ]]; then
		# local UPDATE_JOB="* * */5 * * /usr/bin/curl -s $SCRIPT_URL | /bin/bash >> $LOG_FILE 2>&1 # $CRON_IDENTIFIER" ################# Change back after testing
		local UPDATE_JOB="*/5 * * * * /usr/bin/curl -s $SCRIPT_URL | /bin/bash >> $LOG_FILE 2>&1 # $CRON_IDENTIFIER"  
		(crontab -l ; echo "$UPDATE_JOB") | crontab -
	fi
}


# Function to change the collectd restart time to every hour from once per day so graphs are written to disk hourly
function collectd_restart() {

	# Define cron file path
	FILE="/etc/cron.d/collectd_to_disk"

	# Define the new cron job line
	NEW_CRON_JOB="42 */1 * * * root /bin/systemctl restart collectd"

	if grep -Fxq "$NEW_CRON_JOB" "$FILE"; then
		echo "collectd restart already at 1hr" >> $logfile 2>&1
	else
		# Comment out the old cron job and add the new one
		sed -i 's/^42 23 \* \* \* root \/bin\/systemctl restart collectd/# 42 23 * * * root \/bin\/systemctl restart collectd/' "$FILE"
		echo "" >> "$FILE"
		echo "# every 1 hour" >> "$FILE"
		echo "$NEW_CRON_JOB" >> "$FILE"

		# Verify the changes
		echo "Modified collectd restart cron file:" >> $logfile 2>&1
		cat "$FILE" >> $logfile 2>&1
	fi
}


# Function to change options in the graphs1090 config file
function graphs1090_config() {

	# Define the file path
	FILE="/etc/default/graphs1090"
	
	# Ensure the file exists
	if [ ! -f "$FILE" ]; then
	  echo "File not found: $FILE" >> $logfile 2>&1
	  exit 1
	fi

	# Check if the line starting with colorscheme= exists in the file
	if grep -q "^colorscheme=" "$FILE"; then
		# If it exists, replace it with the desired line
		sed -i "s/^colorscheme=.*/colorscheme=dark/" "$FILE"
	fi

	echo "Updated $FILE to set colorscheme=dark." >> $logfile 2>&1
}


# Function to change options in the tar1090 config file
function tar1090_config() {
	
	file="/usr/local/share/tar1090/html/config.js"

	# Ensure the file exists
	if [ ! -f "$file" ]; then
	  echo "File not found: $file"  >> $logfile 2>&1
	  exit 1
	fi

	# Use sed to uncomment the specified lines to enable route information
	sed -i 's|// useRouteAPI = false;|useRouteAPI = true;|' "$file"
	sed -i 's|// routeApiUrl = "https://api.adsb.lol/api/0/routeset";|routeApiUrl = "https://api.adsb.lol/api/0/routeset";|' "$file"
	echo "Modified $file lines:" >> $logfile 2>&1
	grep "useRouteAPI" "$file" >> $logfile 2>&1
	grep "routeApiUrl" "$file" >> $logfile 2>&1
}


# Function to add options to readsb config to enable heatmap
function readsb_heatmap() {

		file="/etc/default/readsb"

		# Options to be added
		options="--heatmap-dir /var/globe_history --heatmap 30"

		if [ ! -d "/var/globe_history" ]; then
			mkdir /var/globe_history
			chown readsb /var/globe_history
		fi	

		# Check if the file contains a line starting with "JSON_OPTIONS="
		if grep -q '^JSON_OPTIONS=' "$file"; then
			# Get the line starting with "JSON_OPTIONS="
			line=$(grep '^JSON_OPTIONS=' "$file")

			# Check if the options are already present
			if [[ "$line" != *"$options"* ]]; then
				# Append options within the quotes
				new_line=$(echo "$line" | sed "s|\"$| $options\"|")
				# Use sed to replace the old line with the new line
				sed -i "s|^JSON_OPTIONS=.*|$new_line|" "$file"
				echo "Options added successfully - $options" >> $logfile 2>&1
			else
				echo "Options are already present - $options" >> $logfile 2>&1
			fi
		else
			echo "No line starting with 'JSON_OPTIONS=' found in $file." >> $logfile 2>&1
		fi
}


# Function to make various config changes
function config_changes() {
	
	# if this is a new install or new install option supplied
	if [[ $initial_install = true ]]; then
		collectd_restart
		graphs1090_config
		tar1090_config
		readsb-gain 49.6 >> $logfile 2>&1
	fi
	
	# Check if the heatmap=true value was passed on the cmd line
	if [[ $heatmap = true ]]; then
		readsb_heatmap
	fi
}


# ************** Pre-install prep **********************

# Various install prep including asking questions, delete files etc
install_prep

# Read in readsb location from command line or input
read_location

# Read in Device_ID from command line or input
set_device_id

# Delete old unused files from previous installs
del_old_files


# ************** Software Install ***********************

# Step 1: Update package repositories
run_command "Updating package repositories" "apt-get update"


# Step 2: Upgrade installed packages if selected
if [[ $package_update = true ]]; then
    apt-get upgrade -y   | tee -a $logfile
	clear
fi


# Step 3: Install curl and wget if not already installed
run_command "Installing curl if required" "apt-get -y install curl"
run_command "Installing wget if required" "apt-get -y install wget"
run_command "Installing librtlsdr-dev if required" "apt-get -y install librtlsdr-dev"

# Step 4: Download and install readsb and graphs1090
run_command "Installing readsb" \
	"curl -sL https://github.com/wiedehopf/adsb-scripts/raw/master/readsb-install.sh | bash"
	
run_command "Installing graphs1090" \
	"curl -sL https://github.com/wiedehopf/graphs1090/raw/master/install.sh | bash"
	
sed -i -e 's|After=.*|After=vector.service|' /lib/systemd/system/readsb.service >> $logfile 2>&1
# Change readsb service restart to 60sec
sed -i 's/RestartSec=15/RestartSec=60/' /lib/systemd/system/readsb.service >> $logfile 2>&1

# Add connector to vector if not already present	
if grep -q -- "--net-connector localhost,30006,json_out" /etc/default/readsb ; then
	echo "readsb already configured for Wingbits" | tee -a $logfile
else
	sed -i.bak 's/NET_OPTIONS="[^"]*/& '"--net-connector localhost,30006,json_out"'/' /etc/default/readsb >> $logfile 2>&1
	echo "Added Wingbits config to readsb config file" | tee -a $logfile
fi

# Set readsb location and gain

readsb-set-location $antenna_location >> $logfile 2>&1


# Step 5: Install and configure Vector

CSM_MIGRATE=true bash -c "$(curl -sL https://setup.vector.dev)" >> $logfile 2>&1

run_command "Installing vector" \
	"apt-get -y install vector"
  
mkdir -p /etc/vector
touch /etc/vector/vector.yaml
# curl -o /etc/vector/vector.yaml "https://gitlab.com/wingbits/config/-/raw/$WINGBITS_CONFIG_VERSION/vector.yaml" >> $logfile 2>&1  ################# Change back after testing - Do we want to drop the ver for this as well? Otherwise need to make a new 0.0.03 dir and copy in.
curl -o /etc/vector/vector.yaml "https://gitlab.com/wingbits/config/-/raw/0.0.2/vector.yaml" >> $logfile 2>&1
sed -i 's|ExecStart=.*|ExecStart=/usr/bin/vector --watch-config|' /lib/systemd/system/vector.service
echo "DEVICE_ID=\"$device_id\"" > /etc/default/vector
chmod 644 /etc/default/vector


# Step 6: Various config changes
config_changes


# Step 7: Reload systemd daemon, enable and start services
run_command "Starting services" \
  "systemctl daemon-reload" \
  "systemctl enable vector" \
  "systemctl restart readsb vector"


# Step 8: Check if services are online
check_service_status "vector" "readsb"


# Step 9: Create a cron job to check for install script updates every 5 days if selected
update_crontab
	

# Save the new version number now install complete
echo $WINGBITS_CONFIG_VERSION > /etc/wingbits/version
echo
grep "setting L" "$logfile"

echo -e "\n\033[0;32mInstallation complete!\033[0m"
echo
grep "All done!" "$logfile" | sed 's/.*All done!//'
echo
echo -e "\nInstallation log file is available at $logfile."

echo -e "\nIf there is anything unexpected or for further information, including optimization tips, please read through docs.wingbits.com."

echo -e "\n*** Please restart with \"sudo reboot\" if this is your first Wingbits install on this machine. ***"
echo

echo "$(date): Wingbits install end time" >> $logfile


